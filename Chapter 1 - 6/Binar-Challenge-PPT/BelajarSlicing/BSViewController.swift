//
//  BSViewController.swift
//  Binar-Challenge-PPT
//
//  Created by Adrian K on 09/05/22.
//

import UIKit

enum Course: String, CaseIterable, TitleEnum {
    case AKChallenge
    case BelajarTable
    case BelajarChips
    case BelajarForm
    
    static var sorted: [Course] {
        Course.allCases.sorted { $0.rawTitle < $1.rawTitle
        }.map{ $0 }
    }
}

final class BSViewController: UITableViewController, StoryboardInstantiable {
    private let name: String
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(name: String){
        self.name = name
        
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Belajar Slicing"
        tableView.registerCell(UITableViewCell.self)
    }
    
    //MARK: TableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        let numberOfCourse: Int = Course.sorted.count
        return numberOfCourse
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        let row: Int = indexPath.row
        let titleCourse: String = Course.sorted[row].rawTitle
        cell.textLabel?.text = titleCourse
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let row: Int = indexPath.row
        let course: Course = Course.sorted[row]
        
        switch course {
        case .AKChallenge:
            break
        case .BelajarTable:
            open(BSTableViewController())
        case .BelajarChips:
            open(BSChipsViewController())
        case .BelajarForm:
            break
        }
    }
    
    private func open(_ viewController: UIViewController){
        navigationController?.pushViewController(viewController, animated: true)
    }
}
