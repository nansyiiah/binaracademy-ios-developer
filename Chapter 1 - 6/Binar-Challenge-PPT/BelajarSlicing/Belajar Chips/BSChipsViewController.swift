//
//  BSChipsViewController.swift
//  Binar-Challenge-PPT
//
//  Created by Adrian K on 09/05/22.
//

import UIKit

final class LeftAlignmentCollectionLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(
        in rect: CGRect
    ) -> [UICollectionViewLayoutAttributes]? {
        let attributes: [UICollectionViewLayoutAttributes]? = super.layoutAttributesForElements(in: rect)
        guard let _attributes: [UICollectionViewLayoutAttributes] = attributes else {
            return []
        }
        
        var x: CGFloat = sectionInset.left
        var y: CGFloat = -1.0
        
        for attribute in _attributes {
            if attribute.representedElementCategory != .cell { continue }
            
            if attribute.frame.origin.y >= y {
                x = sectionInset.left
            }
            attribute.frame.origin.x = x
            x += attribute.frame.width + minimumInteritemSpacing
            y = attribute.frame.maxY
        }
        
        return _attributes
    }
}


final class BSChipsViewController: UICollectionController {
    private let spacing: CGFloat = 8
    private lazy var words: [String] = ["White", "Black", "Yellow", "Pink"]
    private let screenRect: CGRect = UIScreen.main.bounds
    
    override func loadView() {
        let collectionViewLayout = LeftAlignmentCollectionLayout()
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: spacing)
        collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionView = UICollectionView(
            frame: screenRect,
            collectionViewLayout: collectionViewLayout
        )
        collectionView.registerCell(BSChipsCollectionViewCell.self)
        collectionView.allowsMultipleSelection = true
        collectionView.backgroundColor = .white
        view = collectionView
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: UICollectionViewDataSource
    override func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        let numberOfWords: Int = words.count
        return numberOfWords
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reusableCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BSChipsCollectionViewCell", for: indexPath)
        guard let cell = reusableCell as? BSChipsCollectionViewCell else {
            return reusableCell
        }
        let row: Int = indexPath.row
        let word: String = words[row]
        cell.fill(with: word)
        return cell
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return spacing
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        return spacing
    }
    
    // MARK: UICollectionViewDelegate
    override func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
    }
}

final class BSChipsCollectionViewCell: UICollectionViewCell {
    lazy var textLabel: UILabel = {
        let view = UILabel()
        view.textColor = .systemBlue
        view.textAlignment = .center
        return view
    }()
    
    override var isSelected: Bool {
        didSet {
            didSelect()
        }
    }
    required init?(coder: NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(textLabel)
        textLabel.makeConstraint { view in
            [view.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16),
             view.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -16),
             view.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 4),
             view.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -4)]
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let cornerRad: CGFloat = frame.height/2
        contentView.layer.cornerRadius = cornerRad
        contentView.layer.borderColor = UIColor.systemBlue.cgColor
        contentView.layer.borderWidth = 2
    }
    
    private func didSelect(){
        if isSelected{
            contentView.backgroundColor = .systemBlue
            textLabel.textColor = .white
        }else {
            contentView.backgroundColor = .white
            textLabel.textColor = .systemBlue
        }
    }
    func fill(with string: String){
        textLabel.text = string
    }
}
