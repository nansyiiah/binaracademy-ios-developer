//
//  ViewController.swift
//  Binar-Challenge-PPT
//
//  Created by Adrian K on 18/03/22.
//

import UIKit

class AKViewControllerChallenge3: UIViewController {

    @IBOutlet weak var value1TextField: UITextField!
    @IBOutlet weak var value2TextField: UITextField!
    @IBOutlet weak var sumButton: UIButton!
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var imageLabel: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        value1TextField?.keyboardType = .numberPad
        value2TextField?.keyboardType = .numberPad
        
    }
    
    @IBAction func onTambahButtonTap(_ sender: Any) {
        let value1Text = Int(value1TextField.text!) ?? 0
        let value2Text = Int(value2TextField.text!) ?? 0
        let hasil = value1Text + value2Text
        resultLabel.text = String(hasil)
        let randomImage = [
            UIImage(systemName: "bolt.fill"),
            UIImage(systemName: "flame.fill"),
            UIImage(systemName: "hare.fill"),
            UIImage(systemName: "pawprint.fill"),
            UIImage(systemName: "ant.fill")
        ]
        
        imageLabel.image = randomImage.randomElement()!!
    }
    
}

