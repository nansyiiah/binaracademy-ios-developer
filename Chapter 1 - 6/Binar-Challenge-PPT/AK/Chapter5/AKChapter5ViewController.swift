//
//  AKChapter5ViewController.swift
//  Binar-Challenge-PPT
//
//  Created by Adrian K on 27/04/22.
//

import UIKit

final class AKChapter5ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    @IBOutlet weak var tableView: UITableView!
    
    private let instagramAPI = InstagramAPI(appId: "626837289aabc005ad0f8f31")
    private var displayedUsers: [IGUserShortResponse] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        loadUsers()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let userCount: Int = displayedUsers.count
        return userCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userIdentifier", for: indexPath)
        let row: Int = indexPath.row
        let user: IGUserShortResponse = displayedUsers[row]
        cell.textLabel?.text = "\(user.firstName) \(user.lastName)"
        return cell
    }
    
    private func loadUsers() {
        instagramAPI.getUsers {
            [weak self] (result) in
            guard let _self = self else {
                return
            }
            switch result {
            case let .success(data):
                _self.displayedUsers = data.data
                _self.tableView.reloadData()
            case let .failure(error):
                print(String(describing: error))
            }
        }
    }
}
