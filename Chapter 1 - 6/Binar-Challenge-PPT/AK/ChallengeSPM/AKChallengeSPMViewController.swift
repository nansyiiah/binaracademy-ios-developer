//
//  AKChallengeSPMViewController.swift
//  Binar-Challenge-PPT
//
//  Created by Adrian K on 12/05/22.
//

import UIKit
import Kingfisher

final class AKChallengeSPMViewController: UITableViewController {
    
    private let selectedUser = IGUserShortResponse?.self
    private let instagramAPI = InstagramAPI(appId: "626837289aabc005ad0f8f31")
    private var displayedUsers: [IGUserShortResponse] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUsers()
        tableView.registerCell(AKDetailUserViewCell.self)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let userCount: Int = displayedUsers.count
        return userCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reusableCell: UITableViewCell = tableView.dequeueReusableCell(
            withIdentifier: "AKDetailUserViewCell",
            for: indexPath
        )
        guard let cell = reusableCell as? AKDetailUserViewCell else {
            return reusableCell
        }
        let row: Int = indexPath.row
        let user: IGUserShortResponse = displayedUsers[row]
        cell.textLabel?.text = "\(user.firstName) \(user.lastName)"
        cell.imageView?.loadImage(resource: user.picture)
        return cell
    }

    
    private func loadUsers() {
        instagramAPI.getUsers {
            [weak self] (result) in
            guard let _self = self else {
                return
            }
            switch result {
            case let .success(data):
                _self.displayedUsers = data.data
                _self.tableView.reloadData()
            case let .failure(error):
                print(String(describing: error))
            }
        }
    }

}
