//
//  AKDetailUserViewCell.swift
//  Binar-Challenge-PPT
//
//  Created by Adrian K on 13/05/22.
//

import UIKit

class AKDetailUserViewCell: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fill(with params: IGUserShortResponse) {
//        photoImageView?.loadImage(resource: params.picture)
        titleLabel?.text = params.firstName
    }
    
}

