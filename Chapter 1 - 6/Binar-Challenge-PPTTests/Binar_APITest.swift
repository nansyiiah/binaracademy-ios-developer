//
//  Binar-API.swift
//  Binar-Challenge-PPTTests
//
//  Created by Adrian K on 05/07/22.
//

import XCTest
@testable import Binar_Challenge_PPT

class Binar_APITest: XCTestCase {
    
    func testAKViewControllerChallenge3WholeNumber(){
        let formatter = Binar_API()
        XCTAssertEqual(formatter.string(decimal: 0), "0.00")
//        formatter.string(decimal: 0) == "0"
    }
}
