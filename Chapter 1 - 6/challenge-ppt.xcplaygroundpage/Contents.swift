//: [Previous](@previous)

import Foundation

// Soal
// Data Type
/* Buatlah variable dan konstanta dari data sebagai berikut:
 Pak Asep memesan sebuah buah di toko online yang hanya menerima uang dalam USD. 1 USD saat itu ekuivalen dengan Rp. 14500 (konstanta). Pak Asep memesan buah mangga sebanyak 23 buah. Status dari buah itu tersedia (isAvailable)(boolean).
 */

let uangUSD: Int = 14500
let itemName: String = "Mangga"
let amount: Int = 23
let isAvailable: Bool = true

//Operator
/*
 Dari konstanta dan variable yang sudah dibuat di atas, buatlah perhitungan sesuai dengan cerita berikut:
 Pak Asep pulang ke rumah membawa buah tersebut. Buah yang sudah dibeli tersebut harus dikurangi sebanyak 5 buah untuk diberikan ke Bu Nita. Lalu sisanya diberikan kepada anaknya yang masih SMA sebanyak 5 buah. Sisanya dimasukkan ke dalam kulkas yang sudah ada 4 buah mangga. Pak Asep harus mencocokkan apakah sudah memberikan buah dengan jumlah yang sama kepada Bu Nita dan anaknya. Berapakah jumlah sisa buah Pak Asep?
 */
var buNita: Int = 5
var anakPakAsep: Int = 5
var kulkas: Int = 4
var isSame: Bool = buNita == anakPakAsep
var sisaBuah: Int = amount - buNita - anakPakAsep
var total = sisaBuah + kulkas

// ARRAY
/*
 Buatlah sebuah array yang berisi jumlah buah yang sekarang dimiliki masing-masing dari mereka
 */

let arr: [Int] = [buNita, anakPakAsep, total]

//print(arr)

//Control Flow

//Sekarang, mari kita buat sebuah perulangan
//dengan menggunakan data di bawah ini
//(menggunakan for-in dan while):
var data: [String] = ["Nasi Goreng", "Soto", "Dendeng Balado", "Nasi Uduk", "Nasi Gila"]
for i in data {
    if i == "Dendeng Balado"{
        print("Aku suka makan nasi pakai \(i)")
    }else{
        print(i)
    }
}
print("")
var j = 0

while j < data.count {
    let element = data[j]
    print(element)
    j+=1
}

//If - else
//Dan buatlah sebuah pengkondisian di dalam perulangan yang sudah dibuat dengan situasi sebagai berikut (menggunakan if-else dan guard):

//: [Next](@next)
